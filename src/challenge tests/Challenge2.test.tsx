import { configure, mount } from 'enzyme';
import { configureStore } from '../redux';
import { Provider } from 'react-redux';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import App from '../App';

configure({ adapter: new Adapter() });

jest.useFakeTimers();

describe('Challenge 2: AsyncTracker', () => {
  /**
   * This test will have you use the AsyncTracker component to watch the progress of an asynchronous action dispatch.
   *
   * You will need to dig into the AsyncTracker.tsx file to learn how to use it.
   *
   * Modify the App component such that clicking the #delay-increment-btn element will increment the counter with a 1
   * second delay (using the `delayIncrement` action creator).
   *
   * While the increment is pending, use an AsyncTracker component to
   * display a "Loading" indicator using a span element
   * like so: `<span>Loading...</span>`.
   *
   * Hint: The async ID for the delayIncrement action creator is `delay-increment`.
   *
   * Bonus 1: When the increment is complete, use an AsyncTracker
   * callback to emit a log message with whatever you like.
   *
   * Bonus 2: Find a way to test the logging behavior.
   *
   * Bonus 3: Make the test run faster by using Jest's timer mocks.
   */
  const store = configureStore();
  const app = mount(
    <Provider store={store}>
      <App />
    </Provider>
  );

  const button = app.find('button').last();

  it('Increases the value asyncally', () => {
    expect(app.contains(<span>{1}</span>)).toBeTruthy();
    expect(app.find('#spinner')).toHaveLength(0);
    button.simulate('click');
    expect(app.contains(<span>{1}</span>)).toBeTruthy();
    expect(app.find('#spinner')).toHaveLength(1);

    jest.runTimersToTime(2000);

    app.update();
    expect(app.contains(<span>{2}</span>)).toBeTruthy();
    expect(app.find('#spinner')).toHaveLength(0);
  });

  it('Logs after increasing value', () => {
    button.simulate('click');
    global.console.log = jest.fn();
    expect(global.console.log).not.toHaveBeenCalledWith(
      'Increment complete. Hurray!'
    );
    jest.runTimersToTime(2000);

    expect(global.console.log).toHaveBeenCalledWith(
      'Increment complete. Hurray!'
    );
  });
});
