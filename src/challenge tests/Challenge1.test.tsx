import { configure, mount } from 'enzyme';
import { configureStore } from '../redux';
import { Provider } from 'react-redux';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import App from '../App';

configure({ adapter: new Adapter() });

describe('Challenge 1: dispatching redux actions', () => {
  /**
   * The App component is set up to display the current count for the counter reducer.
   *
   * Modify the App component so that it will increment the counter by **the current count** when the #increment-btn
   * element is clicked.
   *
   * When that's done, unskip this test and run `yarn test` in a console to see the result.
   */
  it('Increases the counter.', () => {
    const store = configureStore();
    const app = mount(
      <Provider store={store}>
        <App />
      </Provider>
    );
    const button = app.find('button').first();
    // console.log(button.debug());
    // expect(button.length).toEqual(1);

    expect(app.contains(<span>{1}</span>)).toBeTruthy();
    button.simulate('click');
    expect(app.contains(<span>{2}</span>)).toBeTruthy();
    button.simulate('click');
    expect(app.contains(<span>{4}</span>)).toBeTruthy();
    button.simulate('click');
    expect(app.contains(<span>{8}</span>)).toBeTruthy();
    button.simulate('click');
    expect(app.contains(<span>{16}</span>)).toBeTruthy();
  });
});
