import * as React from 'react';
import './App.css';
import { RootState } from './redux/reducers';
import { connect, Dispatch } from 'react-redux';
import { actionCreators } from './redux/actions/counter';

import Button from 'material-ui/Button';
import { CircularProgress } from 'material-ui/Progress';

import AsyncTracker from './AsyncTracker';

interface AppProps {}

interface ConnectProps {
  counter: number;
  increment: (currentValue: number) => void;
  asyncIncrement: () => void;
}

type Props = AppProps & ConnectProps;

// const loadingIndicator = (
//   <div>
//     <br />
//     <span>Loading...</span>
//   </div>
// );

const loadingIndicator2 = (
  <div id="spinner">
    <CircularProgress />
  </div>
);

const logComplete = (): void => {
  // find a way to only log it in console
  console.log('Increment complete. Hurray!');
};

export const App: React.SFC<Props> = props => (
  <div className="App">
    <div id="counterLabel">
      <span>{props.counter}</span>
    </div>

    <Button size="large" onClick={() => props.increment(props.counter)}>
      Click to increment
    </Button>
    <Button size="large" onClick={props.asyncIncrement}>
      Click to increment slowly
    </Button>

    <AsyncTracker
      id="delay-increment"
      onResolve={logComplete}
      pendingContent={loadingIndicator2}
    />
  </div>
);

const mapStateToProps = (state: RootState, props: AppProps) => ({
  counter: state.counter.value
});

const mapDispatchToProps = (dispatch: Dispatch<RootState>) => {
  return {
    increment: (currentValue: number) => {
      dispatch(actionCreators.increment(currentValue));
    },
    asyncIncrement: () => {
      dispatch(actionCreators.delayIncrement());
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
